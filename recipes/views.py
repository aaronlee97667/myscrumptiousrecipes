from django.shortcuts import get_object_or_404, render

from .models import Recipe

# Create your views here.

def show_recipe(request, id):
    a_really_cool_recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": a_really_cool_recipe,
    }
    return render(request, "recipes/detail.html", context)


def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
        "something": "something"
    }
    return render(request, "recipes/list.html", context)
