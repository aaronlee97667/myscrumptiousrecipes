from django.urls import path
from recipes.views import recipe_list, show_recipe

urlpatterns = [
    path("", recipe_list),
    path("<int:id>/", show_recipe),
    ]
